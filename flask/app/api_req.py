import flask
import os

from flask import Flask,jsonify,request,abort,Blueprint
import json,redis,logging,uuid,secrets
from flask_negotiate import consumes, produces
from flask_talisman import Talisman
import json
from flask import escape

from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

APP = Flask(__name__)

APP.config['REDIS_PASSWORD'] = "MThE6knPoxtk"
APP.config['REDIS_MSG_EXPIRE'] = os.environ.get('REDIS_MSG_EXPIRE')

limiter = Limiter(APP, key_func=get_remote_address)

REQUEST_API = Blueprint('api_req', __name__)


def get_blueprint():
    """Return the blueprint for the main app module"""
    return REQUEST_API

@REQUEST_API.route('/', methods=['GET'])
def send_welcome():
    return "Please click here to get the swagger file<a href="+"'"+request.url_root+"/swagger#'> swagger </a><br> Please click here to get the postman collection<a href="+"'"+request.url_root+"static/storytel_API_collection.postman_collection.json'> Postman Collection</a>"

@REQUEST_API.route('/viewMessage/<string:_id>', methods=['GET'])

@limiter.limit("10/minute") # maximum of 10 requests per minute. to avoid guessing attacks and burp Turbo Intruder
def view_msg(_id):
    """Get message by the ID
    @param _id: the id
    @return: 200: a valid message from redis \
    with application/json mimetype.
    @raise 404: if message request not found or expired within 7 days
    """
    if _id is None: #check if the _id paramter is empty or not
        abort(400, description="Required parameter is missing")


    redis_connection = redis.Redis(host='my_redis', port=6379, db=0,password=APP.config['REDIS_PASSWORD'],username="myuser")
    if(redis_connection.ping()): #check redis connection
        if not redis_connection.exists(_id): #check if the given id is exists or not
            redis_connection.close()
            abort(403, description="Message Expired") #retrun an error message if the requested key not exsited in the redis
        else:
            get_msg=redis_connection.get(_id) #getting message of the given id
            redis_connection.close()
            return jsonify(
                {
                "Token":str(_id),
                "Message":escape((str(get_msg,'utf-8') )) #return user message 
                }
                )
    else:
            abort(500, description="DB Connection Error") #return an error message, if the db connection unsuccessful 



@REQUEST_API.route('/createMessage', methods=['POST'])
@consumes('application/json')
def CreateMessage():
    """Create a message that lives for 7 days
    @param msg: post : the user's message
    @return: 201: a message with a UUID and link to the message \
    with application/json mimetype.
    @raise 400: misunderstood request
    """
     #https://www.sohamkamani.com/uuid-versions-explained/
    
    #https://docs.python.org/3/library/secrets.html
    SecureUUID=uuid.uuid4()
    SecureToken=secrets.token_urlsafe(8)
    SecureID=str(SecureUUID)+"-"+str(SecureToken) #generate infeasible to guess UUID  and crypto safe random ID. SecureID generates in the backend. so user doesn't have control over this ID

    request.on_json_loading_failed
    data = request.get_json(force=True,silent = True)

    if not data:
        abort(400, description="JSON is not valid")

    if not request.get_json():
        abort(400, description="Requeset should not be empty")

    if not data.get('msg'): #check if the msg paramter given by the user or not
        abort(400 ,description="msg parameter is missing")

    if type(data.get('msg')) != str:
        abort(400 ,description="msg must be a string")

    redis_connection = redis.Redis(host='my_redis', port=6379, db=0,password=APP.config['REDIS_PASSWORD'],username="myuser")
    if(redis_connection.ping()):
        
        userMeassge=str(data.get('msg'))
        if not (redis_connection.exists(SecureID)):
            redis_connection.set(SecureID,str(userMeassge)) #set and store user given message and system generated ID
            redis_connection.expire(SecureID,int(APP.config['REDIS_MSG_EXPIRE'])) #set user message to expire in 7 days
            redis_connection.close()
            return jsonify(
            {
                "Token":str(SecureID),
                "Message":escape(str(userMeassge)),
                "Url":request.url_root+"viewMessage/"+SecureID

            }
        
            ),201
        else:
            abort(500, description="key duplication detected")
    else:
        abort(500, description="DB Connection Error")

