from nose.tools import assert_true
from nose.tools import assert_false
import requests


BASE_URL = "http://127.0.0.1" #uncomment when your running on docker
#BASE_URL = "http://localhost:5000" #uncomment when your running without docker (python app.py mode)
#BASE_URL = "http://143.244.130.36" #uncomment when your running on live env




#Test viewMessage GET Request
def test_Non_existing_key():
    "Test Non existing key"
    response = requests.get(url=BASE_URL+"/viewMessage/sdddds")
    assert_true(response.status_code == 403)

def test_malformed_paramater():
    "Test malformed parameter"
    response = requests.get(url=BASE_URL+"/viewMessage/'s")
    assert_true(response.status_code == 403)

def test_empty_reqest():
    "Test empty GET paramter"
    response = requests.get(url=BASE_URL+"/viewMessage/")
    assert_true(response.status_code == 404)


#Test createMessage POST Request
def test_add_new_record_no_payload():
    "Test adding a new record with no payload"
    payload = None
    response = requests.post('%s/createMessage' % (BASE_URL), json=payload)
    assert_true(response.status_code == 415)


def test_create_new_message_with_strings():
    "Create a new message with strings"
    headers = {'Content-type': 'application/json'}
    payload={"msg":"test"}
    response = requests.post('%s/createMessage' % (BASE_URL), json=payload)
    assert_true(response.status_code == 201)

def test_create_new_message_with_int():
    "Create a new message with int"
    headers = {'Content-type': 'application/json'}
    payload={"msg":1}
    response = requests.post('%s/createMessage' % (BASE_URL), json=payload)
    assert_false(response.status_code == 201)

def test_create_new_message_with_unsupported_content_type():
    "Create a new message with unsupported content-type"
    headers = {'Content-type': 'text/plain'}
    payload={"msg":"test"}
    response = requests.post('%s/createMessage' % (BASE_URL), payload)
    assert_true(response.status_code == 415)

def test_create_new_message_with_wrong_parameter():
    "Create a new message with wrong parameter"
    payload={"msg1":"test"}
    response = requests.post('%s/createMessage' % (BASE_URL), json=payload)
    assert_true(response.status_code == 400)


def test_create_new_message_with_invalid_json():
    "Create a new message with invalid json"
    headers = {'Content-type': 'application/json'}
    payload='{"msg":1-}'
    response = requests.post('%s/createMessage' % (BASE_URL), data=payload,headers = {'Content-type': 'application/json'})
    assert_true(response.status_code == 400)

#Test viewMessage GET Request
def test_rate_limiting():
    "Test Rate limit by sending 25 reqs"
    for i in range(1,25):
        response = requests.get(url=BASE_URL+"/viewMessage/SOMERANDOM")
        if(response.status_code==429):
            break
    assert_true(response.status_code == 429)